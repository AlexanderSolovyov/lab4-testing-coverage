package com.hw.db.DAO;

import com.hw.db.models.Forum;
import com.hw.db.models.User;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ForumDAOTests {

    @Test
    @DisplayName("List of users without since, desc, limit")
    void test_UserList1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        UserDAO.UserMapper USER_MAPPER = new UserDAO.UserMapper();
        ForumDAO.UserList("slug", null, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users" +
                " WHERE forum = (?)::citext" + " ORDER BY nickname;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("List of users with since and limit")
    void test_UserList2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        UserDAO.UserMapper USER_MAPPER = new UserDAO.UserMapper();
        ForumDAO.UserList("slug", 1, "2", null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users" +
                " WHERE forum = (?)::citext" + " AND  nickname > (?)::citext" + " ORDER BY nickname" + " LIMIT ?;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("List of users with since and desc")
    void test_UserList3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        UserDAO.UserMapper USER_MAPPER = new UserDAO.UserMapper();
        ForumDAO.UserList("slug", null, "2", true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users" +
                " WHERE forum = (?)::citext" + " AND  nickname < (?)::citext" + " ORDER BY nickname" + " desc;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }


}
