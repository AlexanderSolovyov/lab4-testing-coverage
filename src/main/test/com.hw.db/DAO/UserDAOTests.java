package com.hw.db.DAO;

import com.hw.db.models.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.*;

public class UserDAOTests {

    @Test
    @DisplayName("change all fields of user")
    void test_UserChange1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("kek", "lol@a.ru", "arbidol", "smth");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("change all user fields but not email")
    void test_UserChange2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("kek", null, "arbidol", "smth");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("change all user fields but not fullname")
    void test_UserChange3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("kek", "lol@a.ru", null, "smth");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
        @DisplayName("change all user fields but not about")
        void test_UserChange4() {
            JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
            UserDAO userDAO = new UserDAO(mockJdbc);
            User user = new User("kek", "lol@a.ru", "smth", null);
            UserDAO.Change(user);
            verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("change email")
    void test_UserChange5() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("kek", "lol@a.ru", null, null);
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("change about")
    void test_UserChange6() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("kek", null, null, "arbidol");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("change fullname")
    void test_UserChange7() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("kek", null, "arbidol", null);
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("change nothing")
    void test_UserChange8() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO userDAO = new UserDAO(mockJdbc);
        User user = new User("kek", null, null, null);
        UserDAO.Change(user);
        verifyNoInteractions(mockJdbc);
    }
}
