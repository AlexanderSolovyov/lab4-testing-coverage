package com.hw.db.DAO;


import com.hw.db.models.Post;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;

import static org.mockito.Mockito.*;

public class PostDAOTests {

    @Test
    @DisplayName("with new author")
    void test_SetPost1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post post = new Post("Author", new Timestamp(1), "forum", "hi", 1, 1, true);
        Post newPost = new Post("NewAuthor", new Timestamp(1), "forum", "hi", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(post);
        PostDAO.setPost(1, newPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("with new message")
    void test_SetPost2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post post = new Post("Author", new Timestamp(1), "forum", "hi", 1, 1, true);
        Post newPost = new Post("Author", new Timestamp(1), "forum", "newHi", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(post);
        PostDAO.setPost(1, newPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("with new time")
    void test_SetPost3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post post = new Post("Author", new Timestamp(1), "forum", "hi", 1, 1, true);
        Post newPost = new Post("Author", new Timestamp(2), "forum", "hi", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(post);
        PostDAO.setPost(1, newPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("with new author and message")
    void test_SetPost4() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post post = new Post("Author", new Timestamp(1), "forum", "hi", 1, 1, true);
        Post newPost = new Post("newAuthor", new Timestamp(1), "forum", "newhi", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(post);
        PostDAO.setPost(1, newPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("with new author and time")
    void test_SetPost5() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post post = new Post("Author", new Timestamp(1), "forum", "hi", 1, 1, true);
        Post newPost = new Post("newAuthor", new Timestamp(2), "forum", "hi", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(post);
        PostDAO.setPost(1, newPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("with new time and message")
    void test_SetPost6() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post post = new Post("Author", new Timestamp(1), "forum", "hi", 1, 1, true);
        Post newPost = new Post("Author", new Timestamp(2), "forum", "newhi", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(post);
        PostDAO.setPost(1, newPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("with new author, time and message")
    void test_SetPost7() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post post = new Post("Author", new Timestamp(1), "forum", "hi", 1, 1, true);
        Post newPost = new Post("newAuthor", new Timestamp(2), "forum", "newhi", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(post);
        PostDAO.setPost(1, newPost);
        verify(mockJdbc).update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("with nothing new")
    void test_SetPost8() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        PostDAO postDAO = new PostDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        Post post = new Post("Author", new Timestamp(1), "forum", "hi", 1, 1, true);
        Post newPost = new Post("Author", new Timestamp(1), "forum", "hi", 1, 1, true);
        when(mockJdbc.queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1))).thenReturn(post);
        PostDAO.setPost(1, newPost);
        verify(mockJdbc).queryForObject(eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), eq(1));
        verifyNoMoreInteractions(mockJdbc);

    }

}
